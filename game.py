import os
import math
import random
import turtle

# register shapes
turtle.register_shape("player.gif")
turtle.register_shape("invader.gif")

# screen
screen = turtle.Screen()
screen.bgcolor("black")
screen.title("Space Invaders")
screen.bgpic("space_invaders_background.gif")  # set background image

# border
border_pen = turtle.Turtle()
border_pen.hideturtle()
border_pen.speed(0)
border_pen.color("white")
border_pen.penup()
border_pen.setposition(-300, -300)
border_pen.pensize(3)
border_pen.pendown()

border_pen.forward(600)  # for loop 으로 바꿔 보자.
border_pen.left(90)
border_pen.forward(600)
border_pen.left(90)
border_pen.forward(600)
border_pen.left(90)
border_pen.forward(600)
border_pen.left(90)

# score
score = 0
score_string = "Score: %s" % score

score_pen = turtle.Turtle()
score_pen.hideturtle()
score_pen.speed(0)
score_pen.color("white")
score_pen.penup()
score_pen.setposition(-290, 280)
score_pen.write(
    score_string,
    False,
    align="left",
    font=("Arial", 14, "normal"))

# player
player = turtle.Turtle()
player.color("blue")
player.shape("player.gif")
player.penup()
player.speed(0)
player.setposition(0, -250)
player.setheading(90)

player_speed = 15

# player event


def move_left():
    x = player.xcor() - player_speed
    if (x < -280):
        x = -280
    player.setx(x)


def move_right():
    x = player.xcor() + player_speed
    if (x > 280):
        x = 280
    player.setx(x)


# enemy group
enemy_speed = 2

enemies = []
for i in range(5):
    enemy = turtle.Turtle()
    enemy.color("red")
    enemy.shape("invader.gif")
    enemy.penup()
    enemy.speed(0)
    x = random.randint(-200, 200)
    y = random.randint(100, 250)
    enemy.setposition(x, y)

    enemies.append(enemy)


# single bullet object
bullet = turtle.Turtle()
bullet.color("yellow")
bullet.shape("triangle")
bullet.penup()
bullet.speed(0)
bullet.setheading(90)
bullet.shapesize(0.5, 0.5)
bullet.hideturtle()

bullet_speed = 20

bullet_state = "ready"


def fire_bullet():
    global bullet_state

    os.system("afplay laser.wav&")

    if bullet_state != "ready":
        return

    x = player.xcor()
    y = player.ycor() + 10
    bullet.setposition(x, y)
    bullet.showturtle()

    bullet_state = "fire"


def is_collision(t1, t2):
    pow_delta_x = math.pow(t1.xcor() - t2.xcor(), 2)
    pow_delta_y = math.pow(t1.ycor() - t2.ycor(), 2)

    distance = math.sqrt(pow_delta_x + pow_delta_y)

    if distance < 15:
        return True
    return False


# register keyboard events
turtle.listen()
turtle.onkeypress(move_left, "Left")
turtle.onkeypress(move_right, "Right")
turtle.onkeypress(fire_bullet, "space")

# game loop
while True:
    for enemy in enemies:
        x = enemy.xcor() + enemy_speed
        enemy.setx(x)

        if (enemy.xcor() > 280):
            enemy_speed = enemy_speed * -1
            for enemy in enemies:
                enemy.sety(enemy.ycor() - 40)

        if (enemy.xcor() < -280):
            enemy_speed = enemy_speed * -1
            for enemy in enemies:
                enemy.sety(enemy.ycor() - 40)

        if is_collision(player, enemy):
            os.system("afplay explosion.wav&")
            player.hideturtle()
            enemy.hideturtle()
            print("Game Over")
            break

    if bullet_state == "fire":
        bullet_y = bullet.ycor() + bullet_speed
        bullet.sety(bullet_y)

        if bullet.ycor() > 275:
            bullet.hideturtle()
            bullet_state = "ready"

        for enemy in enemies:
            if is_collision(bullet, enemy):
                os.system("afplay explosion.wav&")
                enemy.setposition(-200, 250)
                bullet.hideturtle()
                bullet_state = "ready"
                bullet.setposition(0, -400)

                # update the score
                score += 10
                score_string = "Score: %s" % score
                score_pen.clear()  # 여기를 건너띄면?
                score_pen.write(
                    score_string,
                    False,
                    align="left",
                    font=("Arial", 14, "normal"))


# end of file
