[this doc](https://docs.google.com/document/d/1bthF8A55bEyWcnVWVQThFeLRJuo3NtcupPH3rxyBCPo)

<h2>Memo</h2>


새로운 지식에 대한 샘플 제공 

복습 및 습득 지식 활용을 위한 과제 제시

<h3>참고 자료</h3>




*   
cheat sheet - [https://community.computingatschool.org.uk/files/8116/original.pdf](https://community.computingatschool.org.uk/files/8116/original.pdf)


<h2>Lesson 0 - 준비</h2>


goals



*   실행하는 방법
    *   python3 설치
    *   game.py 작성
    *   python3 game.py 로 실행
*   절차
    *   온라인에서 hello python 해보기
    *   로컬에서 해보기
        *   c:\python\spaceinvaders
        *   ~/python/spaceinvaders
        *   
*   부가 정보
    *   vscode
    *   python plugins



<h2>Lesson 1 - 화면 설정, player 등장</h2>



<table>
  <tr>
   <td colspan="2" ><strong>import turtle</strong>
<p>
<strong># screen</strong>
<p>
<strong>screen = turtle.Screen()</strong>
<p>
<strong>screen.bgcolor("black")</strong>
<p>
<strong>screen.title("Space Invaders")</strong>
<p>
<strong># border</strong>
<p>
<strong>border_pen = turtle.Turtle()</strong>
<p>
<strong>border_pen.hideturtle()</strong>
<p>
<strong>border_pen.speed(0)</strong>
<p>
<strong>border_pen.color("white")</strong>
<p>
<strong>border_pen.penup()</strong>
<p>
<strong>border_pen.setposition(-300, -300)</strong>
<p>
<strong>border_pen.pensize(3)</strong>
<p>
<strong>border_pen.pendown()</strong>
<p>
<strong>border_pen.forward(600)  # for loop 으로 바꿔 보자.</strong>
<p>
<strong>border_pen.left(90)</strong>
<p>
<strong>border_pen.forward(600)</strong>
<p>
<strong>border_pen.left(90)</strong>
<p>
<strong>border_pen.forward(600)</strong>
<p>
<strong>border_pen.left(90)</strong>
<p>
<strong>border_pen.forward(600)</strong>
<p>
<strong>border_pen.left(90)</strong>
<p>
<strong># player</strong>
<p>
<strong>player = turtle.Turtle()</strong>
<p>
<strong>player.color("blue")</strong>
<p>
<strong>player.shape("triangle")</strong>
<p>
<strong>player.penup()</strong>
<p>
<strong>player.speed(0)</strong>
<p>
<strong>player.setposition(0, -250)</strong>
<p>
<strong>player.setheading(90)</strong>
<p>
<strong># hold the window</strong>
<p>
<strong>turtle.done()</strong>
   </td>
  </tr>
</table>




Note



1. 파이썬은 코드 해석을 위에서부터 아래로 진행 합니다.
2. # 을 만나면, 이후 내용은 해석을 하지 않습니다. (주석, comment, remark)
3. import 문을 사용하지 않으면, turtle 모듈을 이용할 수 없습니다. \
[https://docs.python.org/3/library/turtle.html](https://docs.python.org/3/library/turtle.html#turtle.speed)
4. pen = turtle.Turtle() # 거북이 객체를 하나 만들어서 pen 이라 부릅니다. \
.shape(모양이름) # “triangle” - 삼각형, “circle” - 원, “turtle” - 거북이 \
.color(색 이름) # “black”, “blue”, “yellow” \
.speed(속도 수치) # 0 - 가장 빠름, 1 - 가장 느림, 3 - 느림, 6- 보통, 10- 빠름 \
.penup() # 펜을 위로 들어 그림이 그려지지 않도록 합니다. \
.pendown() # 펜을 아래로 내려, 그림이 그려지게 합니다. \
.setposition(x, y) # 거북이를 절대좌표 (x,y) 로 이동합니다. 가운데가 (0, 0) 입니다. \
.setheading(degree) # 거북이의 방향을 절대각도 degree 로 바꿉니다. 0-동, 90-북, 180-서, 270-남, 360-동 \
.pensize(size) # 그려지는 펜의 크기를 변경합니다. \
.forward(distance) # distance 만큼 앞으로 이동합니다. \
.left(degree) # 왼쪽으로 상대각도 degree 만큼 회전합니다. \
.right(degree) # 오른쪽으로 상대각도 degree 만큼 회전합니다. \

5. turtle.done() # 그래픽 화면이 닫히는 것을 방지합니다. \

6. --- for loop --- \
for i in range(4): \
    print(i) \
**출력:** \
0 \
1 \
2 \
3 \

7. --- while loop 1 --- \
i = 0 \
while i < 4: \
    print(i) \
    i = i + 1 \
**출력:** \
0 \
1 \
2 \
3 \

8. --- while loop 2 --- \
while gameOver == False: \
    updateEnemy() \
    updatePlayer() \
    updateBackground()   \
 \
 \


<h2>Lesson 2 - player 이동</h2>



<table>
  <tr>
   <td colspan="2" ><em>import turtle</em>
<p>
<em># screen</em>
<p>
<em>screen = turtle.Screen()</em>
<p>
<em>screen.bgcolor("black")</em>
<p>
<em>screen.title("Space Invaders")</em>
<p>
<em># border</em>
<p>
<em>border_pen = turtle.Turtle()</em>
<p>
<em>...</em>
<p>
<em># player</em>
<p>
<em>player = turtle.Turtle()</em>
<p>
<em>player.color("blue")</em>
<p>
<em>player.shape("triangle")</em>
<p>
<em>player.penup()</em>
<p>
<em>player.speed(0)</em>
<p>
<em>player.setposition(0, -250)</em>
<p>
<em>player.setheading(90)</em>
<p>
<strong>player_speed = 15</strong>
<p>
<strong># player event</strong>
<p>
<strong>def move_left():</strong>
<p>
<strong>    x = player.xcor() - player_speed</strong>
<p>
<strong>    if (x < -280):</strong>
<p>
<strong>        x = -280</strong>
<p>
<strong>    player.setx(x)</strong>
<p>
<strong>def move_right():</strong>
<p>
<strong>    x = player.xcor() + player_speed</strong>
<p>
<strong>    if (x > 280):</strong>
<p>
<strong>        x = 280</strong>
<p>
<strong>    player.setx(x)</strong>
<p>
<strong># register keyboard events</strong>
<p>
<strong>turtle.listen()</strong>
<p>
<strong>turtle.onkeypress(move_left, "Left")</strong>
<p>
<strong>turtle.onkeypress(move_right, "Right")</strong>
<p>
<em># hold the window</em>
<p>
<em>turtle.done()</em>
   </td>
  </tr>
</table>






*   
function(함수)를 만들고, 사용해보기


    *   screen 설정하는 부분(세줄)을 함수로 만들고 호출해보자.



*   
엔터키를 누르면, 화면을 노랑으로 변하게 만들어 보자.


*   
화살표(arrow) 오브젝트를 추가하고, 이동시켜 좌표를 screen.title 에 표시해보자.


*   

<h2>Lesson 3 - single enemy, game loop</h2>



<table>
  <tr>
   <td colspan="2" ><em>import turtle</em>
<p>
<em># screen</em>
<p>
<em>...</em>
<p>
<em># player event</em>
<p>
<em>...</em>
<p>
<em>def move_right():</em>
<p>
<em>    x = player.xcor() + player_speed</em>
<p>
<em>    if (x > 280):</em>
<p>
<em>        x = 280</em>
<p>
<em>    player.setx(x)</em>
<p>
<strong># single enemy object</strong>
<p>
<strong>enemy_speed = 2</strong>
<p>
<strong>enemy = turtle.Turtle()</strong>
<p>
<strong>enemy.color("red")</strong>
<p>
<strong>enemy.shape("circle")</strong>
<p>
<strong>enemy.penup()</strong>
<p>
<strong>enemy.speed(0)</strong>
<p>
<strong>enemy.setposition(-200, 250)</strong>
<p>
<em># register keyboard events</em>
<p>
<em>turtle.listen()</em>
<p>
<em>turtle.onkeypress(move_left, "Left")</em>
<p>
<em>turtle.onkeypress(move_right, "Right")</em>
<p>
<strong># game loop</strong>
<p>
<strong>while True:</strong>
<p>
<strong>    x = enemy.xcor() + enemy_speed</strong>
<p>
<strong>    enemy.setx(x)</strong>
<p>
<strong>    if (enemy.xcor() > 280):</strong>
<p>
<strong>        enemy_speed = enemy_speed * -1</strong>
<p>
<strong>        enemy.sety(enemy.ycor() - 40)</strong>
<p>
<strong>    if (enemy.xcor() < -280):</strong>
<p>
<strong>        enemy_speed = enemy_speed * -1</strong>
<p>
<strong>        enemy.sety(enemy.ycor() - 40)</strong>
<p>
<em><del># hold the window</del></em>
<p>
<em><del>turtle.done()</del></em>
<p>
<strong># end of file</strong>
   </td>
  </tr>
</table>




<h2>Lesson 4 - single bullet object</h2>



<table>
  <tr>
   <td>…
<p>
<em>enemy.speed(0)</em>
<p>
<em>enemy.setposition(-200, 250)</em>
<p>
<strong># single bullet object</strong>
<p>
<strong>bullet = turtle.Turtle()</strong>
<p>
<strong>bullet.color("yellow")</strong>
<p>
<strong>bullet.shape("triangle")</strong>
<p>
<strong>bullet.penup()</strong>
<p>
<strong>bullet.speed(0)</strong>
<p>
<strong>bullet.setheading(90)</strong>
<p>
<strong>bullet.shapesize(0.5, 0.5)</strong>
<p>
<strong>bullet.hideturtle()</strong>
<p>
<strong>bullet_speed = 20</strong>
<p>
<strong>bullet_state = "ready"</strong>
<p>
<strong>def fire_bullet():</strong>
<p>
<strong>    global bullet_state</strong>
<p>
<strong>    if bullet_state != "ready":</strong>
<p>
<strong>        return</strong>
<p>
<strong>    x = player.xcor()</strong>
<p>
<strong>    y = player.ycor() + 10</strong>
<p>
<strong>    bullet.setposition(x, y)</strong>
<p>
<strong>    bullet.showturtle()</strong>
<p>
<strong>    bullet_state = "fire"</strong>
<p>
<em># register keyboard events</em>
<p>
<em>turtle.listen()</em>
<p>
<em>turtle.onkeypress(move_left, "Left")</em>
<p>
<em>turtle.onkeypress(move_right, "Right")</em>
<p>
<strong>turtle.onkeypress(fire_bullet, "space")</strong>
   </td>
   <td><em># game loop</em>
<p>
<em>while True:</em>
<p>
<em>    x = enemy.xcor() + enemy_speed</em>
<p>
<em>    enemy.setx(x)</em>
<p>
<em>    if (enemy.xcor() > 280):</em>
<p>
<em>        enemy_speed = enemy_speed * -1</em>
<p>
<em>        enemy.sety(enemy.ycor() - 40)</em>
<p>
<em>    if (enemy.xcor() < -280):</em>
<p>
<em>        enemy_speed = enemy_speed * -1</em>
<p>
<em>        enemy.sety(enemy.ycor() - 40)</em>
<p>
<strong>    if bullet_state == "fire":</strong>
<p>
<strong>        bullet_y = bullet.ycor() + bullet_speed</strong>
<p>
<strong>        bullet.sety(bullet_y)</strong>
<p>
<strong>        if bullet.ycor() > 275:</strong>
<p>
<strong>            bullet.hideturtle()</strong>
<p>
<strong>            bullet_state = "ready"</strong>
<p>
<em># end of file</em>
   </td>
  </tr>
</table>




<h2>Lesson 5 - check collision </h2>



```
import math
import turtle

# screen
...


def fire_bullet():
    global bullet_state

    if bullet_state != "ready":
        return

    x = player.xcor()
    y = player.ycor() + 10
    bullet.setposition(x, y)
    bullet.showturtle()

    bullet_state = "fire"


def is_collision(t1, t2):
    pow_delta_x = math.pow(t1.xcor() - t2.xcor(), 2)
    pow_delta_y = math.pow(t1.ycor() - t2.ycor(), 2)

    distance = math.sqrt(pow_delta_x + pow_delta_y)

    if distance < 15:
        return True
    return False


# register keyboard events
turtle.listen()
turtle.onkeypress(move_left, "Left")
turtle.onkeypress(move_right, "Right")
turtle.onkeypress(fire_bullet, "space")














# game loop
while True:
    x = enemy.xcor() + enemy_speed
    enemy.setx(x)

    if (enemy.xcor() > 280):
        enemy_speed = enemy_speed * -1
        enemy.sety(enemy.ycor() - 40)

    if (enemy.xcor() < -280):
        enemy_speed = enemy_speed * -1
        enemy.sety(enemy.ycor() - 40)

    if is_collision(player, enemy):
        player.hideturtle()
        enemy.hideturtle()
        print("Game Over")
        break

    if bullet_state == "fire":
        bullet_y = bullet.ycor() + bullet_speed
        bullet.sety(bullet_y)

        if bullet.ycor() > 275:
            bullet.hideturtle()
            bullet_state = "ready"

        if is_collision(bullet, enemy):
            enemy.setposition(-200, 250)
            bullet.hideturtle()
            bullet_state = "ready"
            bullet.setposition(0, -400)


# end of file
```


수학 함수



*   
math.sqrt() == square root == 제곱근 \
2 == math.sqrt(4)


*   
math.pow() == power == 제곱 \
9 == math.pow(3, 2)
두 점 사이의 거리 = sqrt(pow(Δx, 2) + pow(Δy, 2))

 

<h2>Lesson 6 - enemies</h2>



<table>
  <tr>
   <td><em>import math</em>
<p>
<strong>import random</strong>
<p>
<em>import turtle</em>
<p>
<em># screen</em>
<p>
<em>...</em>
<p>
<em>def move_right():</em>
<p>
<em>    x = player.xcor() + player_speed</em>
<p>
<em>    if (x > 280):</em>
<p>
<em>        x = 280</em>
<p>
<em>    player.setx(x)</em>
<p>
<em><del># single enemy object</del></em>
<p>
<strong># enemy group</strong>
<p>
<em>enemy_speed = 2</em>
<p>
<strong>enemies = []</strong>
<p>
<strong>for i in range(5):</strong>
<p>
<strong>⇥<em>  enemy = turtle.Turtle()</em></strong>
<p>
<strong>⇥<em>  enemy.color("red")</em></strong>
<p>
<strong>⇥<em>  enemy.shape("circle")</em></strong>
<p>
<strong>⇥<em>  enemy.penup()</em></strong>
<p>
<strong>⇥<em>  enemy.speed(0)</em></strong>
<p>
<strong>⇥  <em><del>enemy.setposition(-200, 250)</del></em></strong>
<p>
<strong>⇥  x = random.randint(-200, 200)</strong>
<p>
<strong>⇥  y = random.randint(100, 250)</strong>
<p>
<strong>⇥  enemy.setposition(x, y)</strong>
<p>
<strong>⇥</strong>
<p>
<strong>⇥  enemies.append(enemy)</strong>
<p>
<em># single bullet object</em>
<p>
<em>...</em>
   </td>
   <td><em># game loop</em>
<p>
<em>while True:</em>
<p>
<strong>    for enemy in enemies:</strong>
<p>
<strong>⇥<em>      x = enemy.xcor() + enemy_speed</em></strong>
<p>
<strong>⇥<em>      enemy.setx(x)</em></strong>
<p>
<strong>⇥</strong>
<p>
<strong>⇥<em>      if (enemy.xcor() > 280):</em></strong>
<p>
<strong>⇥<em>          enemy_speed = enemy_speed * -1</em></strong>
<p>
<strong>⇥<em>          enemy.sety(enemy.ycor() - 40)</em></strong>
<p>
<strong>⇥</strong>
<p>
<strong>⇥<em>      if (enemy.xcor() < -280):</em></strong>
<p>
<strong>⇥<em>          enemy_speed = enemy_speed * -1</em></strong>
<p>
<strong>⇥<em>          enemy.sety(enemy.ycor() - 40)</em></strong>
<p>
<strong>⇥</strong>
<p>
<strong>⇥<em>      if is_collision(player, enemy):</em></strong>
<p>
<strong>⇥<em>          player.hideturtle()</em></strong>
<p>
<strong>⇥<em>          enemy.hideturtle()</em></strong>
<p>
<strong>⇥<em>          print("Game Over")</em></strong>
<p>
<strong>⇥<em>          break</em></strong>
<p>
<em>    if bullet_state == "fire":</em>
<p>
<em>        bullet_y = bullet.ycor() + bullet_speed</em>
<p>
<em>        bullet.sety(bullet_y)</em>
<p>
<em>        if bullet.ycor() > 275:</em>
<p>
<em>            bullet.hideturtle()</em>
<p>
<em>            bullet_state = "ready"</em>
<p>
<strong>        for enemy in enemies:</strong>
<p>
<strong>⇥<em>          if is_collision(bullet, enemy):</em></strong>
<p>
<strong>⇥<em>              enemy.setposition(-200, 250)</em></strong>
<p>
<strong>⇥<em>              bullet.hideturtle()</em></strong>
<p>
<strong>⇥<em>              bullet_state = "ready"</em></strong>
<p>
<strong>⇥<em>              bullet.setposition(0, -400)</em></strong>
<p>
<em># end of file</em>
   </td>
  </tr>
</table>




1. 
적들을 5 x 2 로 줄 맞춰서 배치할 수 있나요? \

<h2>Lesson 7 - drop all enemies</h2>



<table>
  <tr>
   <td colspan="2" ><em>…</em>
<p>
<em># game loop</em>
<p>
<em>while True:</em>
<p>
<em>    for enemy in enemies:</em>
<p>
<em>        x = enemy.xcor() + enemy_speed</em>
<p>
<em>        enemy.setx(x)</em>
<p>
<em>        if (enemy.xcor() > 280):</em>
<p>
<em>            enemy_speed = enemy_speed * -1</em>
<p>
<strong>            for enemy in enemies:</strong>
<p>
<strong>⇥<em>              enemy.sety(enemy.ycor() - 40)</em></strong>
<p>
<em>        if (enemy.xcor() < -280):</em>
<p>
<em>            enemy_speed = enemy_speed * -1</em>
<p>
<strong>            for enemy in enemies:</strong>
<p>
<strong>⇥<em>              enemy.sety(enemy.ycor() - 40)</em></strong>
<p>
<em>        if is_collision(player, enemy):</em>
<p>
<em>            player.hideturtle()</em>
<p>
<em>            enemy.hideturtle()</em>
<p>
<em>            print("Game Over")</em>
<p>
<em>            break</em>
<p>
<em>…</em>
   </td>
  </tr>
</table>


<h2></h2>


<h2>Lesson 8 - score</h2>



<table>
  <tr>
   <td><em>...</em>
<p>
<em># border</em>
<p>
<em>border_pen = turtle.Turtle()</em>
<p>
<em>border_pen.hideturtle()</em>
<p>
<em>border_pen.speed(0)</em>
<p>
<em>…</em>
<p>
<strong># score</strong>
<p>
<strong>score = 0</strong>
<p>
<strong>score_pen = turtle.Turtle()</strong>
<p>
<strong>score_pen.hideturtle()</strong>
<p>
<strong>score_pen.speed(0)</strong>
<p>
<strong>score_pen.color("white")</strong>
<p>
<strong>score_pen.penup()</strong>
<p>
<strong>score_pen.setposition(-290, 280)</strong>
<p>
<strong>score_string = "Score: %s" % score</strong>
<p>
<strong>score_pen.write(</strong>
<p>
<strong>    score_string,</strong>
<p>
<strong>    False,</strong>
<p>
<strong>    align="left",</strong>
<p>
<strong>    font=("Arial", 14, "normal"))</strong>
<p>
<em># player</em>
<p>
<em>...</em>
   </td>
   <td><em># game loop</em>
<p>
<em>while True:</em>
<p>
<em>    for enemy in enemies:</em>
<p>
<em>        ...</em>
<p>
<em>    if bullet_state == "fire":</em>
<p>
<em>        bullet_y = bullet.ycor() + bullet_speed</em>
<p>
<em>        bullet.sety(bullet_y)</em>
<p>
<em>        if bullet.ycor() > 275:</em>
<p>
<em>            bullet.hideturtle()</em>
<p>
<em>            bullet_state = "ready"</em>
<p>
<em>        for enemy in enemies:</em>
<p>
<em>            if is_collision(bullet, enemy):</em>
<p>
<em>                enemy.setposition(-200, 250)</em>
<p>
<em>                bullet.hideturtle()</em>
<p>
<em>                bullet_state = "ready"</em>
<p>
<em>                bullet.setposition(0, -400)</em>
<p>
<strong>                # update the score</strong>
<p>
<strong>                score += 10</strong>
<p>
<strong>                score_string = "Score: %s" % score</strong>
<p>
<strong>                score_pen.clear()  # test</strong>
<p>
<strong>                score_pen.write(</strong>
<p>
<strong>                    score_string,</strong>
<p>
<strong>                    False,</strong>
<p>
<strong>                    align="left",</strong>
<p>
<strong>                    font=("Arial", 14, "normal"))</strong>
<p>
<em># end of file</em>
   </td>
  </tr>
</table>




<h2>Lesson 9 - custom shape</h2>


**# 파일 준비**

**# 1. space_invaders_background.gif**

**# 2. player.gif**

**# 3. enemy.gif**


```
import math
import random
import turtle

# register shapes
turtle.register_shape("player.gif")
turtle.register_shape("invader.gif")

# screen
screen = turtle.Screen()
screen.bgcolor("black")
screen.title("Space Invaders")
screen.bgpic("space_invaders_background.gif")  # set background image

# border
border_pen = turtle.Turtle()
border_pen.hideturtle()
… 

# player
player = turtle.Turtle()
player.color("blue")
player.shape("triangle")
player.shape("player.gif")
player.penup()
player.speed(0)
player.setposition(0, -250)
player.setheading(90)
… 
# enemy group
enemy_speed = 2

enemies = []
for i in range(5):
    enemy = turtle.Turtle()
    enemy.color("red")
    enemy.shape("circle")
    enemy.shape("invader.gif")
    enemy.penup()
    enemy.speed(0)
    x = random.randint(-200, 200)
    y = random.randint(100, 250)


```




<h2>Lesson 10 - play sound</h2>


**# 파일 준비 (Windows 컴퓨터에서는 다른 방법으로 사운드를 재생해야 합니다.**

**# 1. laser.wav**

**# 2. explosion.wav**


```
import os
import math
import random
import turtle

… 

def fire_bullet():
    global bullet_state

    os.system("afplay laser.wav&")

    if bullet_state != "ready":
        return

    x = player.xcor()

# game loop
while True:
… 

        if is_collision(player, enemy):
            os.system("afplay explosion.wav&")
            player.hideturtle()
            enemy.hideturtle()
            print("Game Over")
            break
… 

        for enemy in enemies:
            if is_collision(bullet, enemy):
                os.system("afplay explosion.wav&")
                enemy.setposition(-200, 250)
                bullet.hideturtle()
                bullet_state = "ready"
                bullet.setposition(0, -400)
… 
```



<!-- Docs to Markdown version 1.0β17 -->
